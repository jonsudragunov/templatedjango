

FROM python:3.8.1-alpine3.11

# add need modules
RUN apk add --no-cache --virtual .build-deps gcc musl-dev
RUN apk add --no-cache gettext python-dev py-pip jpeg-dev zlib-dev libpq postgresql-dev

# Define env:
# PYTHONDONTWRITEBYTECODE tels python to not write .pyc files on the import of source modules
ENV PYTHONDONTWRITEBYTECODE=TRUE

# copy our code to docker images path
COPY /. /usr/app/backend

# tell docker we work in our codes map
WORKDIR /usr/app/backend

# Install all dependencies our image require
RUN pip3 install --upgrade pip -r requirements.txt

# remove uneedeed build dependencies
RUN apk del .build-deps

RUN mkdir statics

RUN python manage.py collectstatic --noinput

CMD ["python", "manage.py", "runserver", "0.0.0.0:80"]
